def findSubset(n):
  l = len(n)

  for i in range(1<<l):
    j = 0
    while(i):
      lb = i & 1

      if lb:
        print(n[j], end ="")
      i = i>>1
      j+=1
    print()

if __name__ == "__main__":
  n = "abc"
  print("Sebsets: ", findSubset(n))