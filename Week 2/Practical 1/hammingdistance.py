class Solution:
    def hammingDistance(self, x: int, y: int) -> int:
        result=x^y
        count=0
        while (result>0):
            count+=result&1
            result>>=1
        return count
ob=Solution()
print(ob.hammingDistance(4,1)), 