def toBin(x):
	total=0
	p=1
	while(x):
		lastbit=x&1
		total+=lastbit*p
		p=p*10
		x=x>>1
	return total

print(toBin(8))